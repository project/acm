# API credentials manager

Drupal module to manage API credentials between environments.

[![CircleCI](https://dl.circleci.com/status-badge/img/gh/AlexSkrypnyk/acm/tree/8.x.svg?style=shield)](https://dl.circleci.com/status-badge/redirect/gh/AlexSkrypnyk/acm/tree/8.x)

## Use case

## Features
- Flexible API to define environments, endpoints and credentials fields.
- Integration with [Encrypt](https://www.drupal.org/project/encrypt) module (and [Key](https://www.drupal.org/project/key) module by extent).
